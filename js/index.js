(function($) {
  
  'use strict';
  
	// variables
	var $header_top = $('.header-top');
	var $nav = $('nav');

	// toggle menu 
	$header_top.find('a').on('click', function() {
	  $(this).parent().toggleClass('open-menu');
	});
	
  // variables
  var $isAnimatedSecond = $('.second .is-animated'),
      $isAnimatedSecondSingle = $('.second .is-animated__single'),
      $isAnimatedSecondSingles = $('.second .is-animated__single1'),
      $isAnimatedThird = $('.third .is-animated'),
      $isAnimatedThirdSingle = $('.third .is-animated__single'),
      $isAnimatedFourth = $('.fourth .is-animated'),
      $isAnimatedFourthSingle = $('.fourth .is-animated__single'),
      $isAnimatedFifthp = $('.fifth .is-animated'),
      $isAnimatedFifth = $('.fifth .is-animated-right'),
      $isAnimatedFifthSingle = $('.fifth .is-animated-left');

  // initialize fullPage
  $('#fullpage').fullpage({
	  navigation: true,
	  slidesNavigation: true,
	  controlArrows: false,
	  menu: '#menu',

	  afterLoad: function(anchorLink, index) {

		if(index == 2){
		  $("#main-header-main").css("display","block");
		  $("#main-header-sec2").css("display","none");
		}

		if(index > 2){
		  $("#main-header-main").css("display","block");
		}

		$header_top.css('background', 'rgba(0, 47, 77, .3)');
		if (index == 5) {
			$('#fp-nav').hide();
		  }
	  },
  
  
	  onLeave: function(index, nextIndex, direction) {
    
	   if(index==1 && nextIndex == 2){
		  $("#main-header-sec1").css("display","block");
		  $("#main-header-sec2").css("display","none");
		}

		if(index == 2 && nextIndex == 1){
		  if($("#main-header-sec1").width() != 0){
			$("#main-header-main").css("display","none");
		  }
		  $("#main-header-sec1").css("display","none");
		  $("#main-header-sec2").css("display","block");
		}
		if(index == 5) {
		  $('#fp-nav').show();
		}
	
      /**
      * use the following condition: 
      *
      *   if( index == 1 && direction == 'down' ) {
      *
      * if you haven't enabled the dot navigation
      * or you aren't interested in the animations that occur 
      * when you jump (using the dot navigation) 
      * from the first section to another sections 
      */
      
      // first animation
      if( index == 1 && nextIndex == 2 ) { 
        $isAnimatedSecond.addClass('animated zoomIn'); 
        $isAnimatedSecond.eq(0).css('animation-delay', '.3s');
        $isAnimatedSecond.eq(1).css('animation-delay', '.6s');
        $isAnimatedSecond.eq(2).css('animation-delay', '.9s');
        $isAnimatedSecondSingles.addClass('animated zoomIn').css('animation-delay', '0.6s');
        $isAnimatedSecondSingle.eq(0).addClass('animated fadeInLeftBig').css('animation-delay', '0.3s');
        $isAnimatedSecondSingle.eq(1).addClass('animated fadeInRightBig').css('animation-delay', '0.3s');
      }

    /**
      * use the following condition: 
      *
      *   else if( index == 2 && direction == 'down' ) {
      *
      * if you haven't enabled the dot navigation
      * or you aren't interested in the animations that occur 
      * when you jump (using the dot navigation) from the first section to the third one 
      */
      
      // second animation
      else if( ( index == 1 || index == 2 ) && nextIndex == 3 ) {
        $isAnimatedThird.eq(0).addClass('animated zoomIn').css('animation-delay', '.3s'); 
        $isAnimatedThird.eq(1).addClass('animated zoomIn').css('animation-delay', '.6s');
        $isAnimatedThirdSingle.eq(0).addClass('animated fadeInLeftBig').css('animation-delay', '0.3s');
        $isAnimatedThirdSingle.eq(1).addClass('animated fadeInRightBig').css('animation-delay', '0.3s');
      }

      
     /**
      * use the following condition:
      *
      *   else if( index == 3 && direction == 'down' ) {
      *
      * if you haven't enabled the dot navigation
      * or you aren't interested in the animations that occur 
      * when you jump (using the dot navigation) 
      * from the first or second section to the fourth one 
      */
      
	    // second animation
      else if( ( index == 1 || index == 2 || index == 3 ) && nextIndex == 4 ) {
        $isAnimatedFourth.eq(0).addClass('animated zoomIn').css('animation-delay', '.3s'); 
        $isAnimatedFourth.eq(1).addClass('animated zoomIn').css('animation-delay', '.6s');
        $isAnimatedFourth.eq(2).addClass('animated zoomIn').css('animation-delay', '.9s'); 
        $isAnimatedFourth.eq(3).addClass('animated zoomIn').css('animation-delay', '1.2s');
        $isAnimatedFourth.eq(4).addClass('animated zoomIn').css('animation-delay', '1.5s');
        $isAnimatedFourth.eq(5).addClass('animated zoomIn').css('animation-delay', '1.8s');
        $isAnimatedFourthSingle.addClass('animated bounceInDown').css('animation-delay', '1.2s');
      }
	  
      // third animation
      else if( ( index == 1 || index == 2 || index == 3 || index == 4 ) && nextIndex == 5 ) {
        $isAnimatedFifthp.addClass('animated zoomIn').css('animation-delay', '.3s');
        $isAnimatedFifth.addClass('animated fadeInRightBig').css('animation-delay', '.3s');
        $isAnimatedFifthSingle.addClass('animated fadeInLeftBig').css('animation-delay', '0.3s');
      }
    }

  });
  
})(jQuery);